Fab lab学习第二堂课程之CAD软件学习

We have online guidebook. www.nexmaker.com and offline lab in NexPCB. This lab would arrange training, hardware design, prototype making,project test,etc. We would try to push hardware innovation in our community one by one. In the meantime,We reference Fablab to build our lab. 

Autodesk Fusion 360安装指引


1. 软件的下载与安装
下载页面中需要填写一些基本信息
![](https://gitlab.com/testbedpic12/hellopic/uploads/12b95b45012a474017100fc8cfed8c20/QQ图片20200401174337.png) 
不喜欢透露的就随便填。至少Email可以填个正确的邮箱。

所下载安装包的是11M左右。双击后会自动部署，而软件本体会自动安装到c盘用户文件夹中。

4. 获取license
这一步我忘了具体怎么操作了。网上说是是在官网上，登陆账号，不知道怎么可以申请1年的免费账号

1. 软件界面
软件本身是需要联网的。文件自动备份到云
————————————————
![](https://gitlab.com/testbedpic12/hellopic/uploads/8b4e183276d842a8fd6d2ff4c613888a/QQ图片20200401174557.png)

上图的1是工程目录，主要是存到云服务器中的跟账号有关的工程文件。

2是用户的个人配置入口。

3是作图工具。

4是文件内的元素浏览器

5是坐标。可以用鼠标左键点这个坐标以旋转，也可以按shift+滑轮，以旋转坐标系

参考链接
https://www.nexmaker.com/doc/2cad/Fusion360prepare.html
https://www.autodesk.com/products/fusion-360/overview#banner

下面简单介绍了我用fusion画了一个杯子的过程介绍

主要用到了一个拉伸和扫略的命令（过程的截图我忘记了，不好意思），如下图

![](https://gitlab.com/testbedpic12/hellopic/uploads/e8255f774276d58a58ab2bfc92e2185b/QQ图片20200401180848.png)